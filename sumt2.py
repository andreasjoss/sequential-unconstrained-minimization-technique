#!/usr/bin/python 

#Author:	Andreas Joss (16450612)
#Date:		2015/04/16 

import sys 
sys.path.append('/home/andreas/Documents/Meesters/AD814/')			#add additional path for interepreter to search for Python modules
import optimizationSubroutines as sr						#imports a customized Python module (a Python file which contains variables, functions and classes)
import problems as pb
from sympy import *

		#keep the reload(pr) here until development for pb is completed
reload(pb) 	#reloads the module "pb" as it is only imported the first time it is run from an Python interepreter

p1 = pb.problem("hallo",2)		#instantiates and initializes a new object (a new instance of the "problem" class)
p1.getString()
p1.defineTheoreticalSol([1,2])
p1.addNewSolution([0,0.5])
p1.defineStartingPosition([0,0])

f = p1.x[1]**2 + 3*p1.x[2]**2

p1.defineFunction(f)
