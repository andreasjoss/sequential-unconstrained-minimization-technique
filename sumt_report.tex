\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}					%for advanced mathmematical formulas
\usepackage{graphicx}					%always use this package if you want to insert graphics into your report
\usepackage{fullpage}
\usepackage{changepage}% http://ctan.org/pkg/changepage



\graphicspath{{./graphs/}}


% Title Page
\title{\Huge{Sequential Unconstrained Minimization Technique Assignment}\\[7cm]Advanced Design 814\\[2cm]}

\author{\Large Andreas Joss\\[0.5cm]16450612}
\date{\today}

\begin{document}
\maketitle


\newpage
\section{Introduction}
This report briefly explains the Sequential Unconstrained Minimization Technique (SUMT) which is used for this assignment. This method is implemented in Python code. Various example functions are used to evaluate the successful operation of the code. The results thereof are shown and discussed.  

\section{Theory}
\subsection{Penalty functions}
Until yet, all the previous algorithms considered in this course, are methods for unconstrained optimization problems. Thus until yet, the goal would be to minimize $f(x)$ without any specified constraints. The purpose of a penalty function is to lead the solution in the direction of a feasible region which is known beforehand to the user of this method. If successful, the penalty function gives the illusion of an optimization process which strictly adheres to the given constraints.\bigskip
Consider the general constrained optimization problem as defined in [1]:\bigskip

%\text{minimize } f(\textbf{x}) \text{ with respect to } \textbf{x}\text{ such that}\bigskip
minimize $f(\textbf{x})$ with respect to \textbf{x} such that

\begin{equation}\label{eq:1}
\begin{align*}
g_{j}(\textbf{x}) \leq 0\ \ &j = 1,2...,m\\
h_{j}(\textbf{x}) = 0\ \ &j = 1,2...,r
\end{align*}
\end{equation}

To achieve a constrained optimization problem, a penalty function is formulated by adding terms to the original objective cost function. These extra terms represent penalties for both unequality constraints and equality constraints. The new resulting function, or the penalty function, can then be once again optimized using any unconstrained optimization algorithms according to the liking of the user. The optimization problem then becomes [1]:\bigskip

%\text{minimize } P(\textbf{x}) \text{ with respect to } \textbf{x}\text{ where}\bigskip
minimize $P(\textbf{x},\textbf{$\rho$},\textbf{$\beta$})$ with respect to \textbf{x} where

\begin{equation}\label{eq:2}
\begin{align*}
P(\textbf{x},\textbf{$\rho$},\textbf{$\beta$}) = f(\textbf{x}) + \sum_{j=1}^{r} \rho_{j}h_{j}^{2}(\textbf{x}) + \sum_{j=1}^{m} \beta_{j}g_{j}^{2}(\textbf{x})
\end{align*}
\end{equation}\bigskip

Although the penalty parameter vectors $\boldsymbol{\rho}$ and $\boldsymbol{\beta}$ are problem specific, they adhere to the following conditions:
\begin{equation}\label{eq:3}
\begin{align*}
  \rho_{j} &\gg 0;\\
  %\beta_{j} &=
  \beta_{j} &= \left\{
    \begin{array}{lr}
   0 &       g_{j}(\textbf{x}) \leq 0\\
   \mu_{j} & g_{j}(\textbf{x}) > 0
    \end{array}
    \right.
 \end{align*}
\end{equation}\bigskip

It is common practice to select $\rho_{j} = \rho$ for all $j$, and also select $\mu_{j} = \rho$ for all $j$. A typical value for $\rho$ would be $\rho = 10^{4}$. As $\rho$ is increased, the penalty function becomes more convex. It would be ideal to have $\rho \to \infty$, since this would produce infinite penalties if the penalty function's variables were to fall outside the feasible region, and theoretically one would obtain the most optimum solution $\textbf{x}^{*}$. However this would result in bad computational behaviour due to scaling problems and rounding errors [1]. Depending on how the objective test function is scaled, the penalty parameter $\rho$ can be adjusted in a problem specific manner.  If the algorithm returns garbage solutions due to a large $\rho$ and thus a badly scaled penalty function, $\rho$ can be decreased until feasible values are obtained.

\subsection{Sequential unconstrained minimization technique (SUMT)}
As previously mentioned, if a too large $\rho$ value is used, computational errors can arise[1]. The approach of SUMT, is to solve successive sub-problems each with a larger $\rho$ value. The first sub-problem starts with a relatively small $\rho$ value thus ensuring at least an initial solution, and therafter gradually increasing $\rho$ for each successive sub-problem to achieve the most optimal, yet feasible solution for an optimization problem [1]. The last sub-problem is completed when either the tolerance criteria for convergence is met or when garbage solutions are obtained. The following steps describes the SUMT algorithm as explained by [1]:

\begin{enumerate}
  \item Choose tolerances $\epsilon_{1}$ and $\epsilon_{2}$ and a starting point $\textbf{x}_{k=0}$ (thereby starting at $k=0$). Also choose an initial penalty parameter $\rho_{0}$.
  \item Minimize $P(\textbf{x},\rho_{k})$ using any unconstrained optimization algorithm to the liking of the user. This should deliver the preliminary solution $\textbf{x}^{*}(\rho_{k})$.
  \item Check if the convergence/termination criteria are met for $k>0$, using convergence tests such as:
  terminate if $||\textbf{x}^{*}(\rho_{k}) - \textbf{x}^{*}(\rho_{k-1})|| < \epsilon_{1}$ or/and $|P(\textbf{x}^{*}(\rho_{k-1}) - P(\textbf{x}^{*}(\rho_{k}))| < \epsilon_{2}$\\
  else\\
  set $\rho_{k+1} = c\rho_{k}$ with $c > 1$ and $\textbf{x}^{0} = \textbf{x}^{*}(\rho_{k})$,\\
  set $k = k+1$ and repeat from Step 2.
\end{enumerate}

According to [1], typical values are chosen such that $\rho_{0}=1$ and $c=10$.

\subsection{Code structure}
Regarding the optimization process, the code consists of nested loops. This is illustrated with the following pseudo-code:\bigskip

\begin{adjustwidth}{0cm}{0cm}
SUMT Loop (iterative counter $k$)\\
\{

  \begin{adjustwidth}{2cm}{0cm}
  Conjugate Gradient Method Loop (Polak-Ribiere Plus) (iterative counter $j$)\\
  \{
    \begin{adjustwidth}{2cm}{0cm}
    Golden Section Method Loop (iterative counter $i$)\\
    \{\\
    \}
    \end{adjustwidth}
  \}
  \end{adjustwidth}
\}
\end{adjustwidth}

\section{Results}
The results are displayed using graphs. Within the graphs, two information text boxes are given. Information about the tolerances used, starting position, theoretical solution and Golden Section parameters are given by the upper box. Also included in the upper box is the number of iterations completed (Golden Section, Conjugate Gradient, SUMT) and also the time elapsed since the optimization process is started. The lower box indicates the final solution found by the algorithm. The x-axis shows the SUMT iteration number. Iteration 0 on this axis represents the value of $f(\textbf{x})$ at the given starting position. The value of $f(\textbf{x})$ (thus without any penalty) is shown by the y-axis. Green dots are used to indicate feasible solutions and red dots indicates infeasible solutions. Feasibility is determined by substituting the generated solution into the constraints, and then comparing the value with a small tolerance. The comparison of the substituted constraint function with small tolerances is done because some solutions never quite satisfy the constraint but is very very close of doing so. The coloured dots are also helpful to show if the final solution obtained is indeed feasible or infeasible.\bigskip

The example objective test functions are obtained from [2]. The following results are arbitrary selected example functions and the example numbers (or example names) are named according to the original source [2].

\newpage
\begin{enumerate}
 \item \textit{Example problem 1} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 100(x_{2}-x_{1}^{2})^{2} + (1-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\1 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\1
   \end{bmatrix}
 \end{array}\\
 -1.5 \leq x_{2}$ (unconstrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function1.pdf} 
 \caption{SUMT evaluated by example problem 1}
 \label{fig:test1}
\end{figure} 

Figure \ref{fig:test1} shows gradual convergence to the theoretical solution within the convergence criteria. All the solutions are feasible (hence the green dots) which is probably due to the starting position also being feasible.

\newpage
 \item \textit{Example problem 2} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 100(x_{2}-x_{1}^{2})^{2} + (1-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1.22\\1.5
   \end{bmatrix}
 \end{array}\\
 1.5 \leq x_{2}$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function2.pdf} 
 \caption{SUMT evaluated by example problem 2}
 \label{fig:test2}
\end{figure} 

Figure \ref{fig:test2} indicates successful convergence to the theoretical solution of $x^{*} = [1.22,1.50]$. Also note how the solutions became feasible after starting from an infeasible position (red dot).

\newpage
 \item \textit{Example problem 3} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = x_{2} + 10^{-5}(x_{2}-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   10\\1 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   0\\0
   \end{bmatrix}
 \end{array}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function3.pdf} 
 \caption{SUMT evaluated by example problem 3}
 \label{fig:test3}
\end{figure} 

Figure \ref{fig:test3} shows that some slight minimization has occurred, with $f^{k}(\textbf{x}) = 0.005$ very close to $f^{*}(\textbf{x}) = 0$. Also it can be noted that $x_{2}^{k} \approx x_{2}^{*}$. It is clear that the value of $x_{1}$ has a very limited effect on $f(\textbf{x})$ which explains why the algorithm struggles to find the actual global minimum. By inspection, the function $f(\textbf{x})$ has a term which is scaled by $10^{-5}$ and which is multiplied by the only occurrence of variable $x_{1}$, thereby explaining why this variable is difficult for the algorithm to fine tune. For this example problem the penalty parameter was selected to start as $\rho=1000$ after which the following iterations $\rho_{k+1} = 10\rho_{k}$. This is done to prevent the algorithm from converging too early (while $\rho$ is too small).

\newpage
 \item \textit{Example problem 4} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = \frac{1}{3}(x_{1} + 1)^{3} + x_{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1.125\\0.125 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\0
   \end{bmatrix}
 \end{array}\\
 1 \leq x_{1}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip
 
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function4.pdf} 
 \caption{SUMT evaluated by example problem 4}
 \label{fig:test4}
\end{figure} 

Figure \ref{fig:test4} shows that the starting position is feasible but gradual convergence to the theoretical solution is achieved afterwards. Notice that this problem has two inequality constraints as compared to the previous three example problems which only has one inequality constraint. Also for this example problem, $\rho$ is chosen to start at $\rho=100000$. It is found that a smaller $\rho$ value would converge at an infeasible region, and a too large $\rho$ would cause computational overflow.

\newpage 
 \item \textit{Example problem 5} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = \frac{1}{3}(x_{1} + 1)^{3} + x_{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0\\0 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   -0.55\\-1.55
   \end{bmatrix}
 \end{array}\\
 -1.5 \leq x_{1} \leq 4\\
 -3 \leq x_{2} \leq 3$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function5.pdf} 
 \caption{SUMT evaluated by example problem 5}
 \label{fig:test5}
\end{figure} 

Figure \ref{fig:test5} shows a relatively fast convergence where after the theoretical solution is achieved (and thus a feasible solution). Notice that in this problem, two inequalities are given, but in practise these inequalities need to be divided into four separate inequalities.

\newpage 
 \item \textit{Example problem 6} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (1 - x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -1.2\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\1
   \end{bmatrix}
 \end{array}\\
10(x_{2}-x_{1}^{2}) = 0$\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function6.pdf} 
 \caption{SUMT evaluated by example problem 6}
 \label{fig:test6}
\end{figure} 

This is the first equality example shown. The starting position is an infeasible solution (red dot). Figure \ref{fig:test6} depicts that the theoretical solution is successfully achieved within the convergence criteria and within the feasible area (green dot). 

\newpage 
 \item \textit{Example problem 12} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 0.5x_{1}^{2} + x_{2}^{2} - x_{1}x_{2} - 7x_{1} - 7x_{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0\\0 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   2\\3
   \end{bmatrix}
 \end{array}\\
 25 - 4x_{1}^{2} - x_{2}^{2} \geq 0$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function12.pdf} 
 \caption{SUMT evaluated by example problem 12}
 \label{fig:test12}
\end{figure} 

Notice that this problem has an inequality of quadratic form of both variables $x_{1}$ and $x_{2}$. Figure \ref{fig:test12} shows that the algorithm successfully finds the theoretical global minimum. 

\newpage 
 \item \textit{Example problem 13} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - 2)^{2} + x_{2}^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\-2 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\0
   \end{bmatrix}
 \end{array}\\
 (1-x_{1})^{3}-x_{2} \geq 0\\
 0 \leq x_{1}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function13.pdf} 
 \caption{SUMT evaluated by example problem 13}
 \label{fig:test13}
\end{figure} 

Figure \ref{fig:test13} suggests that this is a rather difficult problem to solve. This is probably due to the complexity of the given constraints for this particular problem. Although the convergence criteria is met, no feasible solutions are produced.

\newpage 
 \item \textit{Example problem 14} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - 2)^{2} + (x_{2}- 1)^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   2\\2 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   0.82\\0.91
   \end{bmatrix}
 \end{array}\\
 -0.25x_{1}^{2}-x_{2}^{2} + 1 \geq 0\\
 x_{1} - 2x_{2} + 1 = 0$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function14.pdf} 
 \caption{SUMT evaluated by example problem 14}
 \label{fig:test14}
\end{figure}

Figure \ref{fig:test14} shows that the starting position is infeasible and that the feasible solution lies at a higher $f(\textbf{x})$ value. The algorithm succeeds to approach the theoretical solution. Notice the quadratic nature of the inequality constraint, and additionally the presence of an equality constraint. For this example problem, it is found that the constrained theoretical solution is only found when the initial $\rho$ is set at $\rho=1000$ and thereafter $\rho_{k+1} = 10\rho_{k}$.

\newpage 
 \item \textit{Example problem 15} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = 100(x_{2} - x_{1}^{2})^{2} + (1-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -2\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   0.5\\2
   \end{bmatrix}
 \end{array}\\
 x_{1}x_{2}-1 \geq 0\\
 x_{1} + x_{2}^{2} \geq 0\\
 x_{1} \leq 0.5$ (constrained minimum)\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function15.pdf} 
 \caption{SUMT evaluated by example problem 15}
 \label{fig:test15}
\end{figure} 

Figure \ref{fig:test15} shows that this problem also proves to be a very challenging optimization problem. No feasible solution is obtained. For this optimization process to at least slighlty approach the theoretical solution a very large starting $\rho$ of $\rho = 10000$ is required. Only two SUMT iterations are allowed since computational overflow occurs when $\rho$ becomes even larger. 

\newpage 
 \item \textit{Example problem 22} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - 2)^{2} + (x_{2} - 1)^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   2\\2 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   1\\1
   \end{bmatrix}
 \end{array}\\
 -x_{1} - x_{2} + 2 \geq 0\\
 -x_{1}^{2} + x_{2} \geq 0$ (constrained minimum)\bigskip
 
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function22.pdf} 
 \caption{SUMT evaluated by example problem 22}
 \label{fig:test22}
\end{figure} 

Figure \ref{fig:test22} shows that the SUMT process successfully converges to the theoretical solution. Notice that at the starting point $f(\textbf{x}^{0}) = 1$ and that the theoretical solution is also $f(\textbf{x}^{*}) = 1$ but the starting position itself is an infeasible solution. The y-axis scale of this particular problem shows the extremely small differences between the $f(\textbf{x}^{0}) = 1$ and $f(\textbf{x}^{*}) = 1$ values. Again a fairly large initial $\rho$ of $\rho=10000$ is required to converge to the theoretical solution.

\newpage 
 \item \textit{Example problem 24} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = \frac{1}{27\sqrt{3}}((x_{1} - 3)^{2} - 9)x_{2}^{3}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1\\0.5 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   3\\ \sqrt{3}
   \end{bmatrix}
 \end{array}\\
 x_{1}/\sqrt{3} - x_{2} \geq 0\\
 x_{1} + \sqrt{3}x_{2} \geq 0\\
 -x_{1} - \sqrt{3}x_{2} +6\geq 0\\
 0 \leq x_{1}\\
 0 \leq x_{2}$ (constrained minimum)\bigskip
 
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function24.pdf} 
 \caption{SUMT evaluated by example problem 24}
 \label{fig:test24}
\end{figure} 

Figure \ref{fig:test24} shows that successful convergence to the theoretical solution occurs even when five inequality constraints are given. The initial $\rho$ value is selected such that $\rho = 100$.

\newpage 
 \item \textit{Example problem 26} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1} - x_{2})^{2} + (x_{2}-x_{3})^{4}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1\\0.5 
   \end{bmatrix} \text{(feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   3\\ \sqrt{3}
   \end{bmatrix}
 \end{array}\\
(1+x_{2}^{2})x_{1} + x_{3}^{4} - 3 = 0$\bigskip
 
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function26.pdf} 
 \caption{SUMT evaluated by example problem 24}
 \label{fig:test26}
\end{figure} 

Figure \ref{fig:test26} shows that the 3-dimensional problem is successfully solved and the solution approaches the theoretical solution.

\newpage 
 \item \textit{Example problem 42} as given by [2]\\[0.2cm]
 $f(\textbf{x}) = (x_{1}-1)^{2} + (x_{2}-2)^{2} + (x_{3}-3)^{2} + (x_{4}-4)^{2} $; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1\\1\\1\\1 
   \end{bmatrix} \text{(not feasible)}\
   \textbf{  x}^{*} = \begin{bmatrix}
   2\\2\\0.6\sqrt{2}\\0.8\sqrt{2}
   \end{bmatrix}
 \end{array}\\
 x_{1} - 2 = 0\\
 x_{3}^{2} + x_{4}^{2} - 2 = 0$\bigskip

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.7]{./graphs/backup_graphs3/function42.pdf} 
 \caption{SUMT evaluated by example problem 42}
 \label{fig:test42}
\end{figure} 

This example is a 4-dimensional problem with two equality constraints. Figure \ref{fig:test42} indicates that the algorithm produces a feasible solution fairly close to the theoretical solution. It is necessary to start this problem with $\rho=10$ to prevent premature convergence to some other local minimum.

\end{enumerate}

\newpage 
\section{Conclusion}
The SUMT algorithm was successfully implemented in Python code. Constrained example test functions were obtained from [2] and were used to verify the correct operation of the code. Most of the test examples implemented were successfully minimized, in particular example functions 1, 2, 4, 5, 6, 12, 14, 22, 24, 26, 42 all converged to the theoretical solution within the convergence criteria of the algorithm. Only example functions 3, 13 and 15 were not able to be minimized to the global minimum.\bigskip

In general, it was found that the SUMT algorithm is not a very rugged optimization process, as most problems required fine tuning of the penalty parameter $\rho$ and convergence tolerances. For some problems if the $\rho$ value is initially too small, then premature convergence could occur at an infeasible region or at some other local minimum. If the initial value of $\rho$ is too large, then computational overflow is likely to occur after the third SUMT iteration (since $\rho$ increases after each SUMT iteration). 

\bigskip

\section{Bibliography}
\begin{enumerate}
 \item Snyman J. Practical Mathmematical Optimization. Technical report, Department of Mechanical and Aeronautical Engineering, University of Pretoria, Pretoria, South Africa, 2005.
 \item Schittkowski K. Test Examples for Nonlinear Programming Codes, Department of Computer Science, University of Bayreuth, Bayreuth, Germany, 2009.
\end{enumerate}

\end{document} 
